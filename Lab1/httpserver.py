#!/usr/bin/env python
#!/usr/bin/python
# -*- coding: utf-8 -*-
""" 
A simple server based on http
"""

from twisted.internet.protocol import Protocol
from twisted.internet import reactor
from  twisted.internet.protocol import ServerFactory

class HttpServer(Protocol):
    #root = '/Users/liujinglun/Desktop/Course/Network_Security/Lab1/http/root'

    def __init__(self):
        pass

    # def extractInfo(self, data):
    #     print data

    def dataReceived(self, data):
        method = data.split()[0]
        print "method:"+ method
        if method != "GET":
            f1 = open(r'/Users/liujinglun/Desktop/Course/Network_Security/Lab1/http/404.html')
            data = f1.read()
            print data
            self.transport.write(data)
        else:
            root = data.split()[1]
            print "root:"+root
            http = data.split()[2]
            print "status:"+http
            #self.transport.write(root)
            #f = open(r'/Users/liujinglun/Desktop/Course/Network_Security/Lab1/http/test.html')
            f = open(r'/Users/liujinglun/Desktop/Course/Network_Security/Lab1/http/'+root)
            #f = open(root)
            data = f.read()
            print data
            #send data to client
            self.transport.write(data)




def main():
    """This runs the protocol on port 8000"""
    factory = ServerFactory()
    factory.protocol = HttpServer
    reactor.listenTCP(8001, factory)
    reactor.run()

if __name__ == '__main__':
    main()