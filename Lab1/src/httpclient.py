#!/usr/bin/env python
#!/usr/bin/python
# -*- coding: utf-8 -*-
""" 
A simple client based on http
"""

from twisted.internet.protocol import Protocol,ClientFactory
from twisted.internet import reactor
import requests
from playground.twisted.endpoints import GateClientEndpoint

class HttpClient(Protocol):


    def __init__(self):
        pass

    # def connectionMade(self):
    #     self.transport.write('hello Echo')

    def connectionMade(self):
        str = raw_input("Enter your file name:");
        str = 'GET '+str+' HTTP/1.0\r\n'
        print str
        self.transport.write(str)
        #self.transport.write("MESSAGE %s\n" % msg)

    def dataReceived(self, data):
        print 'Server said:', data

class HttpClientFactory(ClientFactory):

    def __init__(self):
        pass

    def startedConnecting(self, connector):
        print 'started connecting'

    def buildProtocol(self, addr):
        print 'connected'
        #print addr
        return HttpClient()

    def clientConnectionLost(self, connector, reason):
        print 'Lost connection. Reason:', reason
        print 'prepare to reconnect'
        connector.connect()

    def clientConnectionFailed(self, connector, reason):
        print 'Connection failed. Reason', reason

class Helper:

    def __init__(self):
        pass

def main():
    #f = HttpClientFactory()
    endpoint = GateClientEndpoint(reactor, '20164.0.0.1', 8101, "127.0.0.1", 19090)
    endpoint.connect(HttpClientFactory())
    reactor.run()

if __name__ == '__main__':
	main()
