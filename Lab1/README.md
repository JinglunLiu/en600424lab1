# Lab1 Specification 

## Overview

There are two parts for the lab1, the server file and the client file. The httpclient.py  is designed to send http request to server, and httpserver.py is designed to receive request and send response back to the client. Then the client and server print the content of the target html file.

## Running

### Submission-2

In my private computer, when set the port number as 80, python application needs computer's permission to run the code, so using **sudo python httpserver.py** to run the server part, and then using **sudo python httpclient.py** to run the client part. After the program is running, the user is asked to input the request message to the client, the correct format should be **GET <file name> HTTP1.0**, then client will send the message to the server. If the file is found, it will  be sent back in an HTTP response from server to client and then both server and client will print the content of the file. And the content of the file can also be visites by using the Internet broswer. For example, if the file name is '"test.html", by inputting **localhost:80/test.html**, user can visit the html page in broswer. And If the user did not input the "GET" keyword, there will be a error warning and returned a "404.html" file to show that the input information is incorrect.

### Submission-3

Add playground to the code, the project is in **src** folder, the user do not need to assign local port for the server and the client. 

So the process of running the code is:

> python Chaperone.py
>
> python Gate.py
>
> python httpserver.py
>
> python httpclient.py

## Something Need to be Noticed

I have tried to use the relative path in my code, but there is a error information called "exceptions.IOError: [Error 2] No such file or directory:..", then I used the global path and solve the problem. So maybe it is hard for my code to run on professor's computer. if needed, I will be glad to run the code on my computer and show the result to professor or TA.